"Démo Inkform" by Florian Cargoët (in French)

Include JSEval by Florian Cargoët. [ For calling JS. ]
Include Glk Events by Dannii Willis. [ For hyperlink events. ]

Release along with an interpreter and the "Ink" website.

When play begins:
	[ Test calling an ink-defined function. ]
	let x be a random number from 1 to 9;
	let y be a random number from 1 to 9;
	call ink function "sum" with JSON number x and JSON number y;
	say "Appel de la fonction 'sum' définie dans ink: sum([x], [y]) = [JS value].";
	[ Test setting an ink variable. ]
	let z be a random number from 100 to 999;
	say "Définition de la variable ink 'z' : z = [z].";
	set ink variable "z" to z;

Part 1 - Hyperlinks

To say link (N - number) -- beginning say_link -- running on: (-
	if (glk_gestalt(gestalt_Hyperlinks, 0)) glk_set_hyperlink({N});
-).

To say /link -- ending say_link -- running on: (-
	if (glk_gestalt(gestalt_Hyperlinks, 0)) glk_set_hyperlink(0);
-).

To process hyperlink events: (-
	if (glk_gestalt(gestalt_Hyperlinks, 0)) {
		glk_request_hyperlink_event(gg_mainwin);
		glk_select(gg_event);
	}
-).

Part 2 - JSON helpers

[ Syntax obj => "key" ]
To decide which JSON reference is (obj - JSON reference) => (k - text):
	decide on get key k of obj;

Part 3 - Ink

An Ink scene is a kind of object.
An Ink scene has a text called entry point. [ The ink knot. ]
An Ink scene has a text called exit choice. [ A I7 managed extra choice to leave a conversation. ]

[ I7 things are identifiable by printed name or by ink id. ]
[ You can use this to reference I7 things from Ink. ]
A thing has a text called the ink id.
Ink-identification relates a text (called ID) to a thing (called t) when (ID is the ink id of t or ID is the printed name of t) and ID is not "".
The verb to identify implies the ink-identification relation.

[ Empty rulebook to setup a scene. Use it to send variables to ink. ] 
Before running is an Ink scene based rulebook.

To run ink scene (s - Ink scene):
	if entry point of s is not empty:
		select ink knot (entry point of s);
	now the current ink scene is s;
	follow before running rules for s;
	run ink loop;

Ink story requests input is a truth state that varies.
Current Ink scene is an Ink scene that varies.

To run ink loop:
	continue ink story;
	[
		Loop until there's no more choice to make.
		Since we are blocking, Inform doesn't request a line input until we exit the loop.
	]
	while Ink story requests input is true:
		process hyperlink events;

A glulx input handling rule for a hyperlink-event:
	let index be glk event value 1;
	[ Special link 999 ends the ink story. ]
	if index is 999:
		stop ink story;
	else:
		select ink choice index - 1; [Link indices start at 1 but Ink choices start at 0. ]
		continue ink story;

To stop ink story:
	now Ink story requests input is false;
	JS "removeAllChoices();	";

To continue ink story:
	JS "return continueStory()";
	let paragraphs be JS value => "paragraphs" as a list;
	let choices be JS value => "choices" as a list;
	[ TODO: use activities for this. ]
	repeat with p running through paragraphs:
		let para be p => "text" as a text;
		say para;
		handle tags (p => "tags");
	repeat with choice running through choices:
		let text be choice => "text" as a text;
		let index be choice => "index" as a number;
		say "- [link index + 1][text][/link][line break]";
	if number of entries in choices > 0:
		now Ink story requests input is true;
		[ Add a inform-only choice to stop the ink scene. ]
		if the exit choice of the current ink scene is not "":
			say "- [link 999][exit choice of the current ink scene][/link][line break]";
	else:
		now Ink story requests input is false;

To handle tags (jtags - JSON reference):
	let tags be jtags as a list;
	repeat with jtag running through tags:
		let tag be jtag as a text;
		parse Ink tag tag;
		[ TODO: probably rewrite this as a rulebook. ]
		if tag function is "GIVE":
			if tag arg 1 identifies a person (called receiver) and tag arg 2 identifies a thing (called gift):
				now the receiver has the gift;
			else:
				say "[tag arg 1] => [random thing identified by tag arg 1].";
				say "[tag arg 2] => [random person identified by tag arg 2].";
		else:
			say "Unknown tag '[tag]'.";

[ Parse special tags "# FUNC(arg1, arg2, arg3)" ]
Tag function is a text that varies.
Tag arg 1 is a text that varies.
Tag arg 2 is a text that varies.
Tag arg 3 is a text that varies.
To parse Ink tag (tag - a text):
	now tag function is "";
	now tag arg 1 is "";
	now tag arg 2 is "";
	now tag arg 3 is "";
	[ We remove the spaces around commas and parentheses to simplify the regexp and to have the matches already trimmed. ]
	replace the regular expression " *(<,()>) *" in tag with "\1";
	if tag matches the regular expression "(\w+)\((<^,>*),?(<^,>*),?(<^,>*)\)$":
		now tag function is text matching subexpression 1;
		now tag arg 1 is text matching subexpression 2;
		now tag arg 2 is text matching subexpression 3;
		now tag arg 3 is text matching subexpression 4;

To select ink knot (knot - text):
	JS "story.ChoosePathString('[knot]')";

To call ink function (fn - text) with (arg1 - JSON reference) and (arg2 - JSON reference):
	JS "return story.EvaluateFunction('[fn]', [bracket][arg1], [arg2][close bracket])";

To set ink variable (var - text) to (value - JSON reference):
	JS "story.variablesState[bracket]'[var]'[close bracket] = [value]";

To set ink variable (var - text) to (value - number):
	JS "story.variablesState[bracket]'[var]'[close bracket] = [value]";

To set ink variable (var - text) to (value - text):
	JS "story.variablesState[bracket]'[var]'[close bracket] = '[value]'";

To set ink variable (var - text) to (value - truth state):
	JS "story.variablesState[bracket]'[var]'[close bracket] = [value]";

To select ink choice (i - number):
	JS "
		story.ChooseChoiceIndex([i]);
		removeAllChoices();
	";
	
Part 4 - Save/Restore

ink state is a JSON reference that varies.
To save ink state (this is save ink state):
	JS "return story.state.ToJson()";
	now ink state is JS value;
	preserve JS value; [ Prevent auto-destroy, see JSEval. ]

To restore ink state (this is restore ink state):
	JS "return story.state.LoadJson([ink state])";

[ Call these two phrases from I6. ]
Include (-
[ SAVE_THE_GAME_R res fref;
	if (actor ~= player) rfalse;
	fref = glk_fileref_create_by_prompt($01, $01, 0);
	if (fref == 0) jump SFailed;
	gg_savestr = glk_stream_open_file(fref, $01, GG_SAVESTR_ROCK);
	glk_fileref_destroy(fref);
	if (gg_savestr == 0) jump SFailed;
	((+ save ink state +)-->1)(); ! ADDED: Save ink state.
	@save gg_savestr res;
	if (res == -1) {
		! The player actually just typed "restore". We first have to recover
		! all the Glk objects; the values in our global variables are all wrong.
		GGRecoverObjects();
		glk_stream_close(gg_savestr, 0); ! stream_close
		gg_savestr = 0;
		RESTORE_THE_GAME_RM('B'); new_line;
		((+ restore ink state +)-->1)(); ! ADDED: Restore ink state.
		rtrue;
	}
	glk_stream_close(gg_savestr, 0); ! stream_close
	gg_savestr = 0;
	if (res == 0) { SAVE_THE_GAME_RM('B'); new_line; rtrue; }
	.SFailed;
	SAVE_THE_GAME_RM('A'); new_line;
];
-) instead of "Save The Game Rule" in "Glulx.i6t".


Part 5 - Game

The ink id of the player is "player".

La Démo is a room. "La seule chose à faire ici est de PARLER À NATRIUM pour tester l'intégration ink."
Natrium is a man in la Démo.
La cle rouge is a thing.
The printed name is "clé rouge".
Natrium holds la cle rouge.

Talking to is an action applying to one thing.
Understand "parler a [thing]" as talking to.

Nat chat is an Ink scene
	with entry point "Natrium" [ === Natrium === knot. ]
	and exit choice "Quitter la conversation". [ Extra choice to leave the conversation at any point. ]

[ Expose data to ink. ]
Before running Nat chat:
	set ink variable "i7_natrium_has_key" to whether or not Natrium has la cle rouge;
	set ink variable "i7_player_has_key" to whether or not the player has la cle rouge;

Carry out talking to Natrium:
	run ink scene Nat chat;

Report talking to Natrium:
	if the player has la cle rouge:
		say "(vérifiez votre INVENTAIRE !)[line break]";
	else:
		say "(la prochaine fois, pensez à lui demander la clé !)[line break]";


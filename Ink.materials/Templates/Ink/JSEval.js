(function () {
  const OWNER_PROJECT = "JSEval";
  const JS_EVAL_FILENAME = "JSEval";
  const JS_RETURN_FILENAME = "JSReturn";

  class I7FileWriter {
    constructor(fileName) {
      this.ref = Dialog.file_construct_ref(fileName, "data", "");
      // I7 uses the file header to detect ownership.
      // We use the OWNER_PROJECT constant which need to be the same in the I7 extension.
      this.header = `* //${OWNER_PROJECT}// ${fileName}\n`;
    }
    write(content) {
      const arrayEncoded = Array.from(this.header + content).map((c) =>
        c.charCodeAt(0)
      );

      Dialog.file_write(this.ref, arrayEncoded);
    }
  }

  const evalFile = new I7FileWriter(JS_EVAL_FILENAME);
  const returnFile = new I7FileWriter(JS_RETURN_FILENAME);

  // Needs to exist from the start because I7 checks its existence.
  evalFile.write("");
  returnFile.write("");

  // Wrap Glk.glk_stream_close to intercept write operations on a special file:
  // - JS_EVAL_FILENAME => we evaluate the code instead of writing in the file.
  const original_glk_stream_close = Glk.glk_stream_close;
  Glk.glk_stream_close = function (stream, result) {
    if (stream && stream.type === 1 && stream.writable) {
      if (stream.ref.filename === JS_EVAL_FILENAME) {
        // TODO: do not eval if not ready "*/-"
        evaluate(extractFileBody(stream));
      }
    }
    // Call the original function.
    original_glk_stream_close(stream, result);
  };

  // Decode stream buffer & remove I7 header.
  function extractFileBody(stream) {
    let contents = stream.buf.map((code) => String.fromCharCode(code)).join("");
    // Drop the header.
    return contents.substring(contents.indexOf("\n") + 1);
  }

  // Evaluate JS code, write the result in special file so I7 can read it.
  function evaluate(code) {
    let result;
    try {
      console.log("Evaluating:", code);
      result = new Function("'use strict';\n" + code)();
    } catch (e) {
      console.error(
        "JavaScript code from story file threw an error: " +
          e.message +
          "\n\n" +
          code
      );
    }
    // Write result as JSON in special file.
    returnFile.write(stringify(result));
  }

  function stringify(result) {
    if (result === undefined) {
      // undefined is not stringified (doesn't exist in JSON spec).
      // We replace it with null.
      result = null;
    }
    try {
      return JSON.stringify(result);
    } catch (e) {
      return "null";
    }
  }
})();

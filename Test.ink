VAR z = 0

-> Natrium

=== Natrium ===
VAR i7_natrium_has_key = true
VAR i7_player_has_key = false
{not i7_natrium_has_key: -> get_key_back}
Salut, je m'appelle Natrium. Pour info, la variable ink z a été mise à {z} par Inform.
+   Ça va ?
    Ouais, et toi ?
    ++  Moyen…
        Mince, j'espère que ça ira mieux demain.
    ++  Tout baigne !
        Content de l'apprendre !
    -- À plus !
*   File moi la clé !
    La voilà, malpoli ! # GIVE(player, clé rouge)
-   -> DONE

= get_key_back
Tu pourrais me rendre ma clé ?
+   {i7_player_has_key} La voilà. # GIVE(Natrium, clé rouge)
    Merci !
+   {not i7_player_has_key}Je ne l'ai plus.
    ...
+   Non, donner c'est donner, reprendre c'est voler !
    ...
- -> DONE

=== function sum(x, y) ===
~ return x + y